const _ = require('lodash');

let defaultConfig = {
    app: {
        port: 3000
    },
    db: {
        host: 'localhost',
        port: 27017,
        name: 'off',
        username: 'root',
        password: 'root',
    },
    off_dump_archive_url: 'https://static.openfoodfacts.org/data/openfoodfacts-mongodbdump.gz',
    temp_path: null,
    keep_fields: [],
    auto_refresh: {
        enabled: true,
        cron_expression : '0 3 * * 0',
    },
};

let config;
try {
    let userConfig = require('../config.js');
    config = _.merge(defaultConfig, userConfig);
} catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') {
        throw e;
    }
    config = defaultConfig;
}

module.exports = config;
