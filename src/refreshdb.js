const fs = require('node:fs');
const path = require('node:path');
const util = require('node:util');
const child_process = require('node:child_process');
const stream = require('node:stream');
const request = require('request');
const { rimraf } = require('rimraf');

const exec = util.promisify(child_process.exec);

const dbmanager = require("./db");
const config = require('./config');

function getTempPath() {
    if (config.temp_path) {
        return config.temp_path;
    } else {
        return path.resolve(__dirname, '../tmp');
    }
}

function getDumpArchivePath() {
    return path.resolve(getTempPath(), 'openfoodfacts-mongodbdump.gz');
}

async function refreshDatabaseAsync() {
    const temp_path = getTempPath();
    const archive_path = getDumpArchivePath();

    await rimraf(archive_path);
    await fs.promises.mkdir(temp_path, { recursive: true });

    console.log(`Downloading database dump from '${config.off_dump_archive_url}'.`);

    await stream.promises.finished(request(config.off_dump_archive_url).pipe(fs.createWriteStream(archive_path)));

    await restoreDumpAsync();

    console.log("Deleting tmp files...");
    await rimraf(archive_path);
    console.log("Finished."); 
}

async function restoreDumpAsync() {
    const tmp_collection = 'products_tmp';
    const final_collection = 'products';

    const dumpFile = getDumpArchivePath();
    const dbConfig = config.db;
    
    console.log("Restoring...");
    await exec(`mongorestore --host ${dbConfig.host}:${dbConfig.port} -u ${dbConfig.username} -p ${dbConfig.password} --authenticationDatabase admin --noIndexRestore --nsFrom off.products --nsTo ${dbConfig.name}.${tmp_collection} --drop --gzip --archive=${dumpFile}`, { stdio: 'inherit' });

    const db = await dbmanager.init();

    try {
        if (Array.isArray(config.keep_fields) && config.keep_fields.length)
        {
            let fieldNames = ['code'].concat(config.keep_fields);
            let aggregatedFields = {};
            for (const name of fieldNames) {
                aggregatedFields[name] = 1;
            }
    
            console.log("Shrinking documents...");
            await db.collection(tmp_collection).aggregate([
                { $project : aggregatedFields },
                { $out: final_collection },
            ]).toArray(); // NOTE: toArray is needed to execute the query
            console.log("Documents shrinked.");
            
            console.log("Reindexing...");
            await db.collection(final_collection).createIndex('code');
            console.log("Reindexed.");
    
            console.log(`Deleting '${tmp_collection}'...`);
            await db.collection(tmp_collection).drop();
            console.log(`'${final_collection}' deleted.`);
        }
        else
        {
            console.log("Reindexing...");
            await db.collection(tmp_collection).createIndex('code');
            console.log("Reindexed.");
    
            console.log(`Renaming '${tmp_collection}'...`);
            await db.collection(tmp_collection).rename(final_collection, { dropTarget: true });
            console.log(`Renamed to '${final_collection}'.`);
        }
    
        console.log("Compacting...");
        await db.command({ compact: final_collection });
        console.log("Compacted.");
    } catch (error) {
        await db.collection(tmp_collection).drop();
        throw(error);
    } finally {
        await dbmanager.close();
    }
}

module.exports = {
    getDumpArchivePath,
    refreshDatabaseAsync,
    restoreDumpAsync,
};
